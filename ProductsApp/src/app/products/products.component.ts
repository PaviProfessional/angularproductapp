import { Component, OnInit } from '@angular/core';
import { IProduct } from './product'
import { ProductService } from './product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  productsList: IProduct[] = []
  _listFilter: string = '';
  filteredProducts: any[] = [];

  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.productsList;
  }
  constructor(private productService: ProductService) {

  }


  ngOnInit(): void {
    this.productService.getProducts().subscribe({
      next: products => {
        this.productsList = products;
        this.filteredProducts = this.productsList;
      }
    })
  }
  performFilter(filterBy: string): IProduct[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.productsList.filter((product: IProduct) =>
      product.type.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

}
